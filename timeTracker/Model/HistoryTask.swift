//
//  History.swift
//  timeTracker
//
//  Created by tp on 24/03/2020.
//  Copyright © 2020 lanes. All rights reserved.
//

import Foundation
import CoreData

//Objet historique de tache
class HistoryTask: NSManagedObject{
    
    //Ajoute un historique de tache
    static func add(context: NSManagedObjectContext, idTask: Int, start: Date, end: Date){
        let id = Int32(Helper.increment(context: context, key: "historyTaskId"))
        let newHistoryTask = HistoryTask(context: context)
        newHistoryTask.id = id
        newHistoryTask.idTask = Int32(idTask)
        newHistoryTask.dateStart = start
        newHistoryTask.dateStop = end
        if(newHistoryTask.id != -1){
            do{
                try context.save()
            }catch{
                return
            }
        }
    }
    
    //Retourne un tableau d'historique de tache en donnant l'id de la tache
    static func getByIdTask(context: NSManagedObjectContext, idTask: Int) -> [HistoryTask]{
        let predicate = NSPredicate(format: "idTask == \(Int32(idTask))")
        let request: NSFetchRequest<HistoryTask> = HistoryTask.fetchRequest()
        request.predicate = predicate
        do{
            let results = try context.fetch(request);
            if(results.count > 0){
                return results
            }else{
                return []
            }
        }catch{
            return []
        }
    }
    
    //Retourne l'historique de tache en donnant son id
    static func getById(context: NSManagedObjectContext, id: Int) -> HistoryTask?{
        let predicate = NSPredicate(format: "id == \(id)")
        let request: NSFetchRequest<HistoryTask> = HistoryTask.fetchRequest()
        request.predicate = predicate
        do{
            let results = try context.fetch(request);
            if(results.count > 0){
                return results[0]
            }
        }catch{
            return nil
        }
        return nil
    }
    
    //Change l'id de la tache d'un historique de tache par un autre
    static func addHistoryTasks(context: NSManagedObjectContext, idTaskTarget: Int, idTaskToAdd: Int){
        let historyTasks = self.getByIdTask(context: context, idTask: idTaskToAdd)
        for historyTask in historyTasks{
            historyTask.idTask = Int32(idTaskTarget)
            HistoryTask.update(context: context, historyTask: historyTask)
        }
    }
    
    //Met à jour un historique de tache
    static func update(context: NSManagedObjectContext, historyTask: HistoryTask){
        let predicate = NSPredicate(format: "id == \(historyTask.id)")
        let request: NSFetchRequest<HistoryTask> = HistoryTask.fetchRequest()
        request.predicate = predicate
        do{
            let results = try context.fetch(request);
            if(results.count > 0){
                let oldHistoryTask = results[0]
                    oldHistoryTask.dateStart = historyTask.dateStart
                    oldHistoryTask.dateStop = historyTask.dateStop
                    oldHistoryTask.idTask = historyTask.idTask
                do{
                    try context.save()
                }catch{
                    return
                }
            }
        }catch{
            return
        }
    }
    
    //Supprime les historiques de taches en donnant l'id de leur tache
    static func removeByIdTask(context: NSManagedObjectContext, idTask: Int){
        let predicate = NSPredicate(format: "idTask == \(Int32(idTask))")
        let request: NSFetchRequest<HistoryTask> = HistoryTask.fetchRequest()
        request.predicate = predicate
        do{
            let results = try context.fetch(request);
            if(results.count > 0){
                for historyTask in results{
                    self.remove(context: context, idHistoryTask: Int(historyTask.id))
                }
            }
        }catch{
            return
        }
    }
    
    //Supprime un historique de tache en donnant son id
    static func remove(context: NSManagedObjectContext, idHistoryTask: Int){
        let predicate = NSPredicate(format: "id == \(idHistoryTask)")
        let request: NSFetchRequest<HistoryTask> = HistoryTask.fetchRequest()
        request.predicate = predicate
        do{
            let results = try context.fetch(request);
            if(results.count > 0){
                context.delete(results[0])
                do{
                    try context.save()
                }catch{
                    return
                }
            }
        }catch{
            return
        }
    }
}
