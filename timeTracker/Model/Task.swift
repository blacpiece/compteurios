//
//  Task.swift
//  timeTracker
//
//  Created by tp on 19/03/2020.
//  Copyright © 2020 lanes. All rights reserved.
//

import Foundation
import CoreData

//Objet tache
class Task: NSManagedObject{

    //Ajoute une tache
    static func add(context: NSManagedObjectContext, title: String, idProject: Int, play: Bool, quick: Bool) -> Task?{
        let id = Int32(Helper.increment(context: context, key: "taskId"))
        let newTask = Task(context: context)
        newTask.title = title
        newTask.id = id
        newTask.idProject = Int32(idProject)
        newTask.play = play
        newTask.quick = quick
        if(newTask.id != -1){
            do{
                try context.save()
                return newTask
            }catch{
                return nil
            }
        }
        return nil
    }
    
    //Ajoute la durée d'une tache dans une autre
    static func addTask(context: NSManagedObjectContext, idTaskTarget: Int, idTaskToAdd: Int){
        HistoryTask.addHistoryTasks(context: context, idTaskTarget: idTaskTarget, idTaskToAdd: idTaskToAdd)
        Task.remove(context: context, idTask: idTaskToAdd)
    }
    
    //Suprime une tache et ses historiques
    static func remove(context: NSManagedObjectContext, idTask: Int){
        let predicate = NSPredicate(format: "id == \(idTask)")
        let request: NSFetchRequest<Task> = Task.fetchRequest()
        request.predicate = predicate
        do{
            let results = try context.fetch(request);
            if(results.count > 0){
                HistoryTask.removeByIdTask(context: context,idTask: Int(results[0].id))
                context.delete(results[0])
                do{
                    try context.save()
                }catch{
                    return
                }
            }
        }catch{
            return
        }
    }
    
    //Suprimme les taches d'un projet
    static func removeByIdProject(context: NSManagedObjectContext, idProject: Int){
        let predicate = NSPredicate(format: "idProject == \(Int32(idProject))")
        let request: NSFetchRequest<Task> = Task.fetchRequest()
        request.predicate = predicate
        do{
            let results = try context.fetch(request);
            if(results.count > 0){
                for task in results{
                    self.remove(context: context, idTask: Int(task.id))
                }
            }
        }catch{
            return
        }
    }
    
    //Retourne une tache en donnant son id
    static func getById(context: NSManagedObjectContext, idTask: Int) -> Task?{
        let predicate = NSPredicate(format: "id == \(Int32(idTask))")
        let request: NSFetchRequest<Task> = Task.fetchRequest()
        request.predicate = predicate
        do{
            let results = try context.fetch(request);
            if(results.count > 0){
                return results[0]
            }else{
                return nil
            }
        }catch{
            return nil
        }
    }
    
    //Retourne un tableau de tache en donnant l'id de leur projet
    static func getByIdProject(context: NSManagedObjectContext, idProject: Int) -> [Task]{
        let predicate = NSPredicate(format: "idProject == \(Int32(idProject))")
        let request: NSFetchRequest<Task> = Task.fetchRequest()
        request.predicate = predicate
        do{
            let results = try context.fetch(request);
            if(results.count > 0){
                return results
            }else{
                return []
            }
        }catch{
            return []
        }
    }
    
    //Retourne toutes les taches
    static func getAll(context: NSManagedObjectContext) -> [Task]{
        let request: NSFetchRequest<Task> = Task.fetchRequest()
        do{
            let results = try context.fetch(request);
            if(results.count > 0){
                return results
            }else{
                return []
            }
        }catch{
            return []
        }
    }
    
    //Retourne toutes les taches en cours
    static func getAllPlay(context: NSManagedObjectContext) -> [Task]{
        let predicate = NSPredicate(format: "play == \(true)")
        let request: NSFetchRequest<Task> = Task.fetchRequest()
        request.predicate = predicate
        do{
            let results = try context.fetch(request);
            if(results.count > 0){
                return results
            }else{
                return []
            }
        }catch{
            return []
        }
    }
    
    //Met à jour une tache
    static func update(context: NSManagedObjectContext, task: Task){
        let predicate = NSPredicate(format: "id == \(task.id)")
        let request: NSFetchRequest<Task> = Task.fetchRequest()
        request.predicate = predicate
        do{
            let results = try context.fetch(request);
            if(results.count > 0){
                let oldTask = results[0]
                oldTask.idProject = task.idProject
                oldTask.lastStart = task.lastStart
                oldTask.title = task.title
                oldTask.play = task.play
                oldTask.quick = task.quick
                do{
                    try context.save()
                }catch{
                    return
                }
            }
        }catch{
            return
        }
    }
    
    //Met en pause ou lance une tache
    func pausePlay(context: NSManagedObjectContext){
        if self.play {
            self.play = false
            HistoryTask.add(context: context, idTask: Int(self.id), start: self.lastStart!, end: Date())
        }else{
            self.lastStart = Date()
            self.play = true
        }
    }
    
    //Retourne la durée d'une tache
    func getCount(context: NSManagedObjectContext) -> Int{
        let historyTasks = HistoryTask.getByIdTask(context: context, idTask: Int(self.id))
        var count = 0
        for historyTask in historyTasks{
            if historyTask.dateStop != nil{
                count += -Int(historyTask.dateStart!.timeIntervalSince(historyTask.dateStop!))
            }
        }
        if self.play && self.lastStart != nil{
            count += (-Int(self.lastStart!.timeIntervalSinceNow))
        }
        return count
    }
}
