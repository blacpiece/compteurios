//
//  Project.swift
//  timeTracker
//
//  Created by tp on 17/03/2020.
//  Copyright © 2020 lanes. All rights reserved.
//

import Foundation
import CoreData

//Objet projet
class Project: NSManagedObject{
    
    //Retourne tout les projets sauf les projets spéciaux
    static func getAll(context: NSManagedObjectContext) -> [Project]{
        let predicate = NSPredicate(format: "id != 1 && id != 2")
        let request: NSFetchRequest<Project> = Project.fetchRequest()
        request.predicate = predicate
        do{
            let results = try context.fetch(request);
            if(results.count > 0){
                return results
            }else{
                return []
            }
        }catch{
            return []
        }
    }
    
    //Retourne les projets spéciaux (Alltasks et SingleTasks)
    static func getSpecial(context: NSManagedObjectContext) -> [Project]{
        let predicate = NSPredicate(format: "id == 1 || id == 2")
        let request: NSFetchRequest<Project> = Project.fetchRequest()
        request.predicate = predicate
        do{
            let results = try context.fetch(request);
            if(results.count > 0){
                return results
            }else{
                return []
            }
        }catch{
            return []
        }
    }
    
    //Retourne un projet en donnant son id
    static func getById(context: NSManagedObjectContext, idProject: Int) -> Project?{
        let predicate = NSPredicate(format: "id == \(Int32(idProject))")
        let request: NSFetchRequest<Project> = Project.fetchRequest()
        request.predicate = predicate
        do{
            let results = try context.fetch(request);
            if(results.count > 0){
                return results[0]
            }else{
                return nil
            }
        }catch{
            return nil
        }
    }
    
    //Créé un nouveau projet
    static func add(context: NSManagedObjectContext, title: String){
        let id = Int32(Helper.increment(context: context, key: "projectId"))
        let newProject = Project(context: context)
        newProject.title = title
        newProject.id = id
        if(newProject.id != -1){
            do{
                try context.save()
            }catch{
                return
            }
        }
    }
    
    //Supprime un projet et ses taches en donnant son id
    static func remove(context: NSManagedObjectContext, id: Int){
        let predicate = NSPredicate(format: "id == \(id)")
        let request: NSFetchRequest<Project> = Project.fetchRequest()
        request.predicate = predicate
        do{
            let results = try context.fetch(request);
            if(results.count > 0){
                Task.removeByIdProject(context: context, idProject: Int(results[0].id))
                context.delete(results[0])
                do{
                    try context.save()
                }catch{
                    return
                }
            }
        }catch{
            return
        }
    }
    
    //Met à jour un projet
    static func update(context: NSManagedObjectContext, project: Project){
        let predicate = NSPredicate(format: "id == \(project.id)")
        let request: NSFetchRequest<Project> = Project.fetchRequest()
        request.predicate = predicate
        do{
            let results = try context.fetch(request);
            if(results.count > 0){
                let oldProject = results[0]
                oldProject.title = project.title
                do{
                    try context.save()
                }catch{
                    return
                }
            }
        }catch{
            return
        }
    }
    
    //Retourne la durée des taches dans le projet
    func getTimer(context: NSManagedObjectContext) -> Int{
        if self.id != 1{
            var timer = 0
            let tasks = Task.getByIdProject(context: context, idProject: Int(self.id))
            for task in tasks {
                timer += task.getCount(context: context)
            }
            return timer
        }else{
            return Project.getTimerAll(context: context)
        }
    }
    
    //Retourne la durée de tous les projets
    static func getTimerAll(context: NSManagedObjectContext) -> Int{
        var timer = 0
        let projects = Project.getAll(context: context)
        for project in projects {
            timer += project.getTimer(context: context)
        }
        timer += Project.getById(context: context, idProject: 2)?.getTimer(context: context) ?? 0
        return timer
    }
    
    //Retourne le nombre de tache en cours dans le projet
    func getNbPlay(context: NSManagedObjectContext) -> Int{
        if self.id != 1{
            var nb = 0
            let tasks = Task.getByIdProject(context: context, idProject: Int(self.id))
            for task in tasks {
                if task.play {
                    nb += 1
                }
            }
            return nb
        }else{
            return self.getNbPlayAll(context: context)
        }
    }
    
    //Retourne le nombre de tache en cours dans tous les projets
    func getNbPlayAll(context: NSManagedObjectContext) -> Int{
        var nb = 0
        let projects = Project.getAll(context: context)
        for project in projects {
            nb += project.getNbPlay(context: context)
        }
        nb += Project.getById(context: context, idProject: 2)?.getNbPlay(context: context) ?? 0
        return nb
    }
    
}
