//
//  Helper.swift
//  timeTracker
//
//  Created by tp on 18/03/2020.
//  Copyright © 2020 lanes. All rights reserved.
//

import Foundation
import CoreData
//Objet helper pour créé automatiquement des ids uniques
class Helper: NSManagedObject{

    //Retourne l'id correspondant à la clé donnée
    static func get(context: NSManagedObjectContext, key: String) -> Int{
        let predicate = NSPredicate(format: "key = %@", key)
        let request: NSFetchRequest<Helper> = Helper.fetchRequest()
        request.predicate = predicate
        do{
            let results = try context.fetch(request);
            if(results.count > 0){
                return Int(results[0].val)
            }else{
                self.add(context: context, key: key, val: 1)
                return 1
            }
        }catch{
            return -1
        }
    }
    
    //Ajout d'une clé
    static func add(context: NSManagedObjectContext, key: String, val: Int){
        let helper = Helper(context: context)
        helper.key = key
        helper.val = Int32(val)
        do{
            try context.save()
        }catch{
            return
        }
    }
    
    //Incrémente une clé
    static func increment(context: NSManagedObjectContext, key: String)->Int{
        let predicate = NSPredicate(format: "key = %@", key)
        let request: NSFetchRequest<Helper> = Helper.fetchRequest()
        request.predicate = predicate
        do{
            let results = try context.fetch(request);
            if(results.count > 0){
                let helper = results[0]
                helper.val = helper.val + Int32(1)
                do{
                    try context.save()
                    return Int(helper.val)
                }catch{
                    return -1
                }
            }else{
                self.add(context: context, key: key, val: 1)
                return 1
            }
        }catch{
            return -1
        }
    }
    
}
