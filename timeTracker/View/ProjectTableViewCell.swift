//
//  ProjectTableViewCell.swift
//  timeTracker
//
//  Created by tp on 19/03/2020.
//  Copyright © 2020 lanes. All rights reserved.
//

import UIKit
//Cellule pour afficher un projet
class ProjectTableViewCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var compteur: UILabel!
    @IBOutlet weak var projectImage: UIImageView!
    
    var timer: Timer!
    var count: Int!
    var nbPlay: Int!
    
    //Configuration de la cellule
    func configure(project: Project, timerTasks: Int, nbPlay: Int){
        self.title.text = project.title
        self.count = timerTasks
        self.nbPlay = nbPlay
        self.stop()
        self.start()
        self.projectImage.image = UIImage(named: "folder.png")
        if project.id == 1 {
            self.projectImage.image = UIImage(named: "files.png")
        }
        if project.id == 2 {
            self.projectImage.image = UIImage(named: "file.png")
        }
        if(nbPlay > 0){
            compteur.textColor = UIColor.red
        }else{
            compteur.textColor = UIColor.black
        }
    }
    
    //Démarre le timer
    func start(){
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(increment), userInfo: nil, repeats: true)
    }
    
    //Arrete le timer
    func stop(){
        if timer != nil{
            timer.invalidate()
        }
        self.compteur.text = self.getCompteurInString()
    }
    
    //Retourne le compteur en format hh:mm:ss
    func getCompteurInString()->String{
        return "\(self.getHeureInString()):\(self.getMinInString()):\(self.getSecInString())"
    }
    
    func getHeure()->Int{
        return self.count/3600
    }
    
    func getMin()->Int{
        return (self.count-(3600*(self.count/3600)))/60
    }
    
    func getSec()->Int{
        return self.count-(60*(self.count/60))
    }
    
    func getHeureInString()->String{
        var res = String(self.getHeure())
        if(res.count == 1){
            res = "0\(res)"
        }
        return res
    }
    
    func getMinInString()->String{
        var res = String(self.getMin())
        if(res.count == 1){
            res = "0\(res)"
        }
        return res
    }
    
    func getSecInString()->String{
        var res = String(self.getSec())
        if(res.count == 1){
            res = "0\(res)"
        }
        return res
    }
    
    //Incremente le compteur du nombre de taches en cours dans le projet
    @objc func increment(){
        self.count+=1*nbPlay
        self.compteur.text = self.getCompteurInString()
    }
}
