//
//  HistoryTaskTableViewCell.swift
//  timeTracker
//
//  Created by tp on 15/04/2020.
//  Copyright © 2020 lanes. All rights reserved.
//

import UIKit

//Cellule pour afficher un historique de tache
class HistoryTaskTableViewCell: UITableViewCell {
    
    var index: IndexPath?
    var count: Int!
    var timerProgress: Timer!
    @IBOutlet weak var startEnd: UILabel!
    @IBOutlet weak var timer: UILabel!
    
    //Configuration de la cellule
    func configure(historyTask: HistoryTask){
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        if historyTask.dateStop != nil {
            self.startEnd.text = formatter.string(from: historyTask.dateStart!) + " -> " + formatter.string(from: historyTask.dateStop!)
            self.count = -Int(historyTask.dateStart!.timeIntervalSince(historyTask.dateStop!))
            self.stop()
            timer.textColor = UIColor.black
        }else{
            self.startEnd.text = formatter.string(from: historyTask.dateStart!) + " -> In progress"
            self.count = -Int(historyTask.dateStart!.timeIntervalSinceNow)
            self.stop()
            self.start()
            timer.textColor = UIColor.red
        }
    }
    
    //Démarre le timer
    func start(){
        timerProgress = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(increment), userInfo: nil, repeats: true)
    }
    
    //Arrete le timer
    func stop(){
        if timerProgress != nil{
            timerProgress.invalidate()
        }
        self.timer.text = self.getCompteurInString()
    }
    
    func getCompteurInString()->String{
        return "\(self.getHeureInString()):\(self.getMinInString()):\(self.getSecInString())"
    }
    
    func getHeure()->Int{
        return self.count/3600
    }
    
    func getMin()->Int{
        return (self.count-(3600*(self.count/3600)))/60
    }
    
    func getSec()->Int{
        return self.count-(60*(self.count/60))
    }
    
    func getHeureInString()->String{
        var res = String(self.getHeure())
        if(res.count == 1){
            res = "0\(res)"
        }
        return res
    }
    
    func getMinInString()->String{
        var res = String(self.getMin())
        if(res.count == 1){
            res = "0\(res)"
        }
        return res
    }
    
    func getSecInString()->String{
        var res = String(self.getSec())
        if(res.count == 1){
            res = "0\(res)"
        }
        return res
    }
    
    //Incremente le compteur de l'historique de tache
    @objc func increment(){
        self.count+=1
        self.timer.text = self.getCompteurInString()
    }
}
