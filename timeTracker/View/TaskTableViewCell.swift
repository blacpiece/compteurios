//
//  TaskTableViewCell.swift
//  timeTracker
//
//  Created by tp on 19/03/2020.
//  Copyright © 2020 lanes. All rights reserved.
//

import UIKit
import CoreData

//Protocole pour arreter ou démarre le timer d'une tache
protocol TaskTableViewProtocol {
    func onClickPausePlay(indexPath: IndexPath, cell: TaskTableViewCell)
}

//Cellule pour afficher une tache
class TaskTableViewCell: UITableViewCell {
    
    var index: IndexPath?
    var taskTableViewCellDelegate: TaskTableViewProtocol?
    var timer: Timer!
    var count: Int!
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var compteur: UILabel!
    @IBOutlet weak var buttonPausePlay: UIButton!
    @IBAction func clickButton(_ sender: UIButton) {
        taskTableViewCellDelegate?.onClickPausePlay(indexPath: index!, cell: self)
    }

    //Configuration de la cellule
    func configure(context: NSManagedObjectContext, delegate: TaskTableViewProtocol, index: IndexPath, task: Task){
        if task.play {
            self.count = task.getCount(context: context)
            self.stop()
            if !task.quick{
                self.buttonPausePlay.setImage(UIImage(named: "pause.png"), for: .normal)
            }else{
                self.buttonPausePlay.setImage(UIImage(named: "pauseQuick.png"), for: .normal)
            }
            self.start()
            compteur.textColor = UIColor.red
        }else{
            self.count = task.getCount(context: context)
            self.stop()
            self.buttonPausePlay.setImage(UIImage(named: "play.png"), for: .normal)
            compteur.textColor = UIColor.black
        }
        self.taskTableViewCellDelegate = delegate
        self.index = index
        self.title.text = task.title
    }
    
    //Démarre le timer
    func start(){
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(increment), userInfo: nil, repeats: true)
    }
    
    //Arrete le timer
    func stop(){
        if timer != nil{
            timer.invalidate()
        }
        self.compteur.text = self.getCompteurInString()
    }
    
    //Retourne le compteur en format hh:mm:ss
    func getCompteurInString()->String{
        return "\(self.getHeureInString()):\(self.getMinInString()):\(self.getSecInString())"
    }
    
    func getHeure()->Int{
        return self.count/3600
    }
    
    func getMin()->Int{
        return (self.count-(3600*(self.count/3600)))/60
    }
    
    func getSec()->Int{
        return self.count-(60*(self.count/60))
    }
    
    func getHeureInString()->String{
        var res = String(self.getHeure())
        if(res.count == 1){
            res = "0\(res)"
        }
        return res
    }
    
    func getMinInString()->String{
        var res = String(self.getMin())
        if(res.count == 1){
            res = "0\(res)"
        }
        return res
    }
    
    func getSecInString()->String{
        var res = String(self.getSec())
        if(res.count == 1){
            res = "0\(res)"
        }
        return res
    }
    
    //Incremente le compteur de la tache
    @objc func increment(){
        self.count+=1
        self.compteur.text = self.getCompteurInString()
    }
}
