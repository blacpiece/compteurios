//
//  EditProjectViewController.swift
//  timeTracker
//
//  Created by tp on 16/04/2020.
//  Copyright © 2020 lanes. All rights reserved.
//

import Foundation
import UIKit
import CoreData

//Classe correspondante à l'écran d'édition de projet
class EditProjectViewController: UIViewController, UITextFieldDelegate {
    
    var idProject: Int = 0
    var project: Project?
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext

    @IBOutlet weak var titleText: UITextField!
    //Action save
    @IBAction func saveButtonBarAction(_ sender: UIBarButtonItem) {
        self.save()
    }
    //Disparition du clavier en tapant à coté
    @IBAction func dismissKeyboard(_ sender: UITapGestureRecognizer) {
        self.titleText.resignFirstResponder()
    }
    
    //Sauvegarde les changements
    private func save(){
        if titleText.text != "" {
            self.project?.title = titleText.text
            Project.update(context: context, project: self.project!)
            navigationController?.popViewController(animated: true)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.project = Project.getById(context: context, idProject: self.idProject)
        self.titleText.text = self.project!.title!
    }
    
    //Action bouton return
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.titleText.resignFirstResponder()
        self.save()
        return true
    }
}
