//
//  EditTaskViewController.swift
//  timeTracker
//
//  Created by tp on 19/03/2020.
//  Copyright © 2020 lanes. All rights reserved.
//

import Foundation
import UIKit
import CoreData

//Classe correspondante à l'écran d'édition d'une tache
class EditTaskViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    
    var idTask: Int = 0
    var task: Task?
    var projects: [Project] = []
    var pickerData: [String] = []
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    

    @IBOutlet weak var titleText: UITextField!
    @IBOutlet weak var projectTitlePicker: UIPickerView!
    //Disparition du clavier en tapant à coté
    @IBAction func dismissKeyboard(_ sender: UITapGestureRecognizer) {
        self.titleText.resignFirstResponder()
    }
    //Action save
    @IBAction func saveBarButtonAction(_ sender: UIBarButtonItem) {
        self.task?.title = titleText.text
        let project = projects[self.projectTitlePicker.selectedRow(inComponent: 0)]
        self.task?.idProject = project.id
        self.task?.quick = false
        Task.update(context: context, task: self.task!)
        navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.projectTitlePicker.delegate = self
        self.projectTitlePicker.dataSource = self
        self.setup()
    }
    
    //Paramétrage
    private func setup(){
        var index = 0
        self.task = Task.getById(context: context, idTask: idTask)
        if(self.task != nil){
            self.titleText.text = self.task?.title
        }
        projects = []
        projects.append(Project.getById(context: context, idProject: 2)!)
        let temp = Project.getAll(context: context)
        if(temp.count != 0){
            projects.append(contentsOf: temp)
        }
        pickerData = []
        var selectIndex = 0
        for project in projects{
            pickerData.append(project.title!)
            if project.id == self.task?.idProject{
                selectIndex = index
            }
            index += 1
        }
        projectTitlePicker.reloadAllComponents()
        projectTitlePicker.selectRow(selectIndex, inComponent: 0, animated: true)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return self.pickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?{
        return self.pickerData[row]
    }
    
    //Action bouton return
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.titleText.resignFirstResponder()
        return true
    }
}
