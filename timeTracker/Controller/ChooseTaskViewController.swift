//
//  ChooseTaskViewController.swift
//  timeTracker
//
//  Created by tp on 13/04/2020.
//  Copyright © 2020 lanes. All rights reserved.
//

import Foundation
import UIKit
import CoreData

//Classe correspondante à l'écran de choix pour fusionner 2 taches
class ChooseTaskViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    var idTask: Int = 0
    var tasks: [Task] = []
    var pickerData: [String] = []
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    @IBOutlet weak var taskPicker: UIPickerView!
    //action save
    @IBAction func saveBarButtonAction(_ sender: UIBarButtonItem) {
        let taskToAdd = Task.getById(context: context, idTask: self.idTask)
        let taskTarget = tasks[self.taskPicker.selectedRow(inComponent: 0)]
        if (taskToAdd?.play)!{
            taskToAdd?.pausePlay(context: context)
        }
        Task.addTask(context: context, idTaskTarget: Int(taskTarget.id), idTaskToAdd: Int(taskToAdd!.id))
        navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.taskPicker.delegate = self
        self.taskPicker.dataSource = self
        self.setup()
    }
    
    //Setup
    private func setup(){
        self.pickerData = []
        self.tasks = Task.getAll(context: context)
        for tempTask in tasks{
            pickerData.append(tempTask.title!)
        }
        taskPicker.reloadAllComponents()
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return self.pickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?{
        return self.pickerData[row]
    }
}
