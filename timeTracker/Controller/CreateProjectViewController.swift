//
//  CreateProject.swift
//  project1
//
//  Created by tp on 17/03/2020.
//  Copyright © 2020 lanes. All rights reserved.
//

import UIKit
import CoreData

//Classe correspondante à l'écran de création de projet
class CreateProjectViewController: UIViewController, UITextFieldDelegate {
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    @IBOutlet weak var titleText: UITextField!
    //Disparition du clavier en tapant à coté
    @IBAction func dismissKeyboard(_ sender: UITapGestureRecognizer) {
        self.titleText.resignFirstResponder()
    }
    //Action save
    @IBAction func clicButton(_ sender: UIBarButtonItem) {
       self.save()
    }
    
    //Sauvegarde les changements
    private func save(){
        if titleText.text != "" {
            Project.add(context: context, title: titleText.text!)
            navigationController?.popViewController(animated: true)
        }
    }
    
    //Action bouton return
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.titleText.resignFirstResponder()
        self.save()
        return true
    }
}
