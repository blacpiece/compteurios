//
//  CreateTaskViewController.swift
//  timeTracker
//
//  Created by tp on 19/03/2020.
//  Copyright © 2020 lanes. All rights reserved.
//

import UIKit
import CoreData

//Classe correspondante à l'écran de création d'une tache
class CreateTaskViewController: UIViewController, UITextFieldDelegate {
    
    var projectId: Int = 0
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    @IBOutlet weak var titleText: UITextField!
    //Disparition du clavier en tapant à coté
    @IBAction func dismissKeyboard(_ sender: UITapGestureRecognizer) {
        self.titleText.resignFirstResponder()
    }
    //Action save
    @IBAction func clickButton(_ sender: UIBarButtonItem) {
        self.save()
    }
    
    //Sauvegarde les changements
    private func save(){
        if titleText.text != "" {
            Task.add(context: context, title: titleText.text!, idProject: projectId, play: false, quick: false)
            navigationController?.popViewController(animated: true)
        }
    }
    
    //Action bouton return
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.titleText.resignFirstResponder()
        self.save()
        return true
    }
}
