//
//  Overview.swift
//  project1
//
//  Created by tp on 17/03/2020.
//  Copyright © 2020 lanes. All rights reserved.
//

import UIKit
import CoreData

//Classe correspondante à l'écran d'overview
class OverviewViewController: UIViewController{
    
    var projects: [Project] = []
    var projectsSpecial: [Project] = []
    var tempIdTask: Int = 0
    var tasks: [Task] = []
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    @IBOutlet weak var projectTableView: UITableView!
    @IBOutlet weak var taskTableView: UITableView!
    @IBOutlet weak var editBarButton: UIBarButtonItem!
    @IBOutlet weak var projectSpecialTableView: UITableView!
    //Action quickstart
    @IBAction func quickStartButtonAction(_ sender: UIButton) {
        let tasksInSingleTask = Task.getByIdProject(context: context, idProject: 2)
        let task = Task.add(context: context, title: "Quick_Task_\(tasksInSingleTask.count+1)", idProject: 2, play: false, quick: true)
        if task != nil{
            task!.pausePlay(context: context)
            Task.update(context: context, task: task!)
            self.loadProjectsSpecial()
            self.loadTasks()
            projectSpecialTableView.reloadData()
            taskTableView.reloadData()
        }
    }
    //Action edit
    @IBAction func editBarButtonAction(_ sender: UIBarButtonItem) {
        if self.projectTableView.isEditing{
            editBarButton.title = "Edit"
            self.projectTableView.isEditing = false
            self.taskTableView.isEditing = false
        }else{
            editBarButton.title = "Done"
            self.projectTableView.isEditing = true
            self.taskTableView.isEditing = true
        }
    }
    
    //Lancement app
    override func viewDidLoad() {
        self.setupApp()
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.taskTableView.allowsSelectionDuringEditing = true
        self.projectTableView.allowsSelectionDuringEditing = true
        self.loadAll()
    }
    
    //Recupère les projets spéciaux dans la BDD
    private func loadProjectsSpecial(){
        self.projectsSpecial = Project.getSpecial(context: context)
        projectSpecialTableView.reloadData()
    }
    
    //Recupère les projets dans la BDD
    private func loadProjects(){
        self.projects = Project.getAll(context: context)
        projectTableView.reloadData()
    }
    
    //Recupère les tasks dans la BDD
    private func loadTasks(){
        self.tasks = Task.getAllPlay(context: context)
        taskTableView.reloadData()
    }
    
    //Recharge les 3 table views
    private func loadAll(){
        self.loadTasks()
        self.loadProjects()
        self.loadProjectsSpecial()
    }
    
    //Paramétrage application
    private func setupApp(){
        if(Project.getById(context: context, idProject: 1) == nil){
            Project.add(context: context, title: "All Tasks")
        }
        if(Project.getById(context: context, idProject: 2) == nil){
            Project.add(context: context, title: "Single Tasks")
        }
    }
    
    //Choix action pour quick task
    private func quickChoose(idTask: Int){
        self.tempIdTask = idTask
        let actionSheet = UIActionSheet(title: " ", delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, otherButtonTitles: "Create new task", "Add to task", "Delete")
        actionSheet.show(in: self.view)
    }
}

extension OverviewViewController: UITableViewDataSource {
    //Retourne le nombre de section de la tableView
    func tableView(_ tableView: UITableView, numberOfSections section: Int) -> Int {
        return 1;
    }
    
    //return le nombre de cell par section
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.projectTableView {
            return self.projects.count
        }
        if tableView == self.projectSpecialTableView {
            return self.projectsSpecial.count
        }
        if tableView == self.taskTableView {
            return self.tasks.count
        }
        return 0
    }
    
    //return le contenu d'une cell
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.projectTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProjectCell", for: indexPath) as? ProjectTableViewCell
            let project = self.projects[indexPath.row]
            cell?.configure(project: project, timerTasks: project.getTimer(context: context), nbPlay: project.getNbPlay(context: context))
            return cell!
        }
        if tableView == self.projectSpecialTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProjectCell", for: indexPath) as? ProjectTableViewCell
            let project = self.projectsSpecial[indexPath.row]
            cell?.configure(project: project, timerTasks: project.getTimer(context: context), nbPlay: project.getNbPlay(context: context))
            return cell!
        }
        if tableView == self.taskTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TaskCell", for: indexPath) as? TaskTableViewCell
            let task = self.tasks[indexPath.row]
            cell?.configure(context: context, delegate: self, index: indexPath, task: task)
            return cell!
        }
        return UITableViewCell()
    }
}

extension OverviewViewController: UITableViewDelegate {
    
    //Action lors de la sélection d'une ligne
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == self.projectTableView{
            let project = projects[indexPath.item]
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            projectTableView.deselectRow(at: indexPath, animated: true)
            if self.projectTableView.isEditing {
                let projectVC = storyboard.instantiateViewController(withIdentifier: "EditProjectViewController") as! EditProjectViewController
                projectVC.idProject = Int(project.id)
                self.navigationController?.pushViewController(projectVC, animated: true)
            }else{
                let projectVC = storyboard.instantiateViewController(withIdentifier: "ProjectViewController") as! ProjectViewController
                projectVC.titleProject = project.title!
                projectVC.idProject = Int(project.id)
                self.navigationController?.pushViewController(projectVC, animated: true)
            }
        }
        if tableView == self.projectSpecialTableView{
            let project = projectsSpecial[indexPath.item]
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            if(project.id != 1){
                let projectVC = storyboard.instantiateViewController(withIdentifier: "ProjectViewController") as! ProjectViewController
                projectSpecialTableView.deselectRow(at: indexPath, animated: true)
                projectVC.titleProject = project.title!
                projectVC.idProject = Int(project.id)
                self.navigationController?.pushViewController(projectVC, animated: true)
            }else{
                let allTasksVC = storyboard.instantiateViewController(withIdentifier: "AllTasksViewController") as! AllTasksViewController
                self.navigationController?.pushViewController(allTasksVC, animated: true)
            }
        }
        if tableView == self.taskTableView {
            let task = tasks[indexPath.item]
            if !task.quick{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                taskTableView.deselectRow(at: indexPath, animated: true)
                if self.taskTableView.isEditing {
                    let editTaskVC = storyboard.instantiateViewController(withIdentifier: "EditTaskViewController") as! EditTaskViewController
                    editTaskVC.idTask = Int(task.id)
                    self.navigationController?.pushViewController(editTaskVC, animated: true)
                }else{
                    let historyVC = storyboard.instantiateViewController(withIdentifier: "HistoryViewController") as! HistoryViewController
                    historyVC.idTask = Int(task.id)
                    self.navigationController?.pushViewController(historyVC, animated: true)
                }
            }else{
                self.quickChoose(idTask: Int(task.id))
            }
        }
    }
    
    //Action lors d'une action d'édition
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete{
            if tableView == self.taskTableView {
                let task = tasks[indexPath.item]
                Task.remove(context: context, idTask: Int(task.id))
                self.loadAll()
            }
            if tableView == self.projectTableView{
                let project = projects[indexPath.item]
                Project.remove(context: context, id: Int(project.id))
                self.loadAll()
            }
        }
    }

}

extension OverviewViewController: TaskTableViewProtocol {
    //Protocole lancement/arrêt d'une tache
    func onClickPausePlay(indexPath: IndexPath, cell: TaskTableViewCell) {
        let task = tasks[indexPath.item]
        if !task.quick{
            task.pausePlay(context: context)
            Task.update(context: context, task: task)
            self.loadAll()
        }else{
            self.quickChoose(idTask: Int(task.id))
        }
    }
}

extension OverviewViewController: UIActionSheetDelegate {
    //Action pour une quick task
    func actionSheet(_ actionSheet: UIActionSheet, clickedButtonAt buttonIndex: Int) {
        switch buttonIndex {
        case 1:
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let editTaskVC = storyboard.instantiateViewController(withIdentifier: "EditTaskViewController") as! EditTaskViewController
            editTaskVC.idTask = self.tempIdTask
            let temp = Task.getById(context: context, idTask: self.tempIdTask)
            if temp!.play{
                temp!.pausePlay(context: context)
            }
            self.navigationController?.pushViewController(editTaskVC, animated: true)
        case 2:
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let chooseTaskVC = storyboard.instantiateViewController(withIdentifier: "ChooseTaskViewController") as! ChooseTaskViewController
            chooseTaskVC.idTask = self.tempIdTask
            self.navigationController?.pushViewController(chooseTaskVC, animated: true)
        case 3:
            Task.remove(context: context, idTask: self.tempIdTask)
            self.loadAll()
        default:
            return
        }
    }
}



