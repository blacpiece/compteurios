//
//  AllTasksViewController.swift
//  timeTracker
//
//  Created by tp on 11/04/2020.
//  Copyright © 2020 lanes. All rights reserved.
//

import UIKit
import CoreData

//Classe correspondante à l'écran d'affichage de toutes les taches
class AllTasksViewController: UIViewController {
    
    var tempIdTask: Int = 0
    var tasks: [[Task]] = []
    var headSection: [String] = []
    var nbInSection: [Int] = []
    var idProjectInSection: [Int] = []
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext

    @IBOutlet weak var taskTableView: UITableView!
    @IBOutlet weak var editBarButton: UIBarButtonItem!
    //Action edit
    @IBAction func editBarButtonAction(_ sender: UIBarButtonItem) {
        if self.taskTableView.isEditing{
            editBarButton.title = "Edit"
            self.taskTableView.isEditing = false
        }else{
            editBarButton.title = "Done"
            self.taskTableView.isEditing = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.taskTableView.allowsSelectionDuringEditing = true
        self.loadTasks()
    }
    
    //Recupère les tasks dans la BDD
    private func loadTasks(){
        var projects: [Project] = []
        projects.append(Project.getById(context: context, idProject: 2)!)
        let temp = Project.getAll(context: context)
        if(temp.count != 0){
            projects.append(contentsOf: temp)
        }
        self.headSection = []
        self.nbInSection = []
        self.idProjectInSection = []
        self.tasks = []
        var temp2: [Task]
        for project in projects {
            self.headSection.append(project.title!)
            temp2 = Task.getByIdProject(context: context, idProject: Int(project.id))
            self.nbInSection.append(temp2.count)
            self.idProjectInSection.append(Int(project.id))
            if(temp2.count != 0){
                self.tasks.append(temp2)
            }else{
                self.tasks.append([])
            }
        }
        taskTableView.reloadData()
    }
    
    //Récupère la task correspondante à un indexPath
    private func getTaskFromIndexPath(indexPath: IndexPath)-> Task?{
        if self.tasks.indices.contains(indexPath.section){
            let tempTasks = self.tasks[indexPath.section]
            if tempTasks.indices.contains(indexPath.row){
                let task = tempTasks[indexPath.row]
                return task
            }
        }
        return nil
    }
    
    //Choix action pour quick task
    private func quickChoose(idTask: Int){
        self.tempIdTask = idTask
        let actionSheet = UIActionSheet(title: " ", delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, otherButtonTitles: "Create new task", "Add to task", "Delete")
        actionSheet.show(in: self.view)
    }
}

extension AllTasksViewController: UITableViewDataSource {
    //Retourne le nombre de section de la tableView
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.headSection.count;
    }
    
    //Retourne le nom de la section
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if self.headSection.indices.contains(section){
            return self.headSection[section]
        }else{
            return "Project"
        }
    }
    
    //return le nombre de cell par section
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.nbInSection.indices.contains(section){
            return self.nbInSection[section]
        }else{
            return 0
        }
    }
    
    //return le contenu d'une cell
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TaskCell", for: indexPath) as? TaskTableViewCell
        let task = self.getTaskFromIndexPath(indexPath: indexPath)
        if task != nil{
            cell?.configure(context: context, delegate: self as TaskTableViewProtocol, index: indexPath, task: task!)
        }
        return cell!
    }
}

extension AllTasksViewController: UITableViewDelegate {
    //Action lors de la sélection d'une ligne
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let task = self.getTaskFromIndexPath(indexPath: indexPath)
        if task != nil{
            if !task!.quick{
                if(self.taskTableView.isEditing){
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let editTaskVC = storyboard.instantiateViewController(withIdentifier: "EditTaskViewController") as! EditTaskViewController
                    taskTableView.deselectRow(at: indexPath, animated: true)
                    editTaskVC.idTask = Int(task!.id)
                    self.navigationController?.pushViewController(editTaskVC, animated: true)
                }else{
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let historyVC = storyboard.instantiateViewController(withIdentifier: "HistoryViewController") as! HistoryViewController
                    taskTableView.deselectRow(at: indexPath, animated: true)
                    historyVC.idTask = Int(task!.id)
                    self.navigationController?.pushViewController(historyVC, animated: true)
                }
            }else{
                self.quickChoose(idTask: Int(task!.id))
            }
        }
    }
    
    //Action lors d'une action d'édition
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        let task = self.getTaskFromIndexPath(indexPath: indexPath)
        if task != nil{
            if editingStyle == .delete{
                Task.remove(context: context, idTask: Int(task!.id))
                self.loadTasks()
            }
        }
    }
    
    //Action lors d'un déplacement de ligne
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let task = self.getTaskFromIndexPath(indexPath: sourceIndexPath)
        if task != nil{
            if self.idProjectInSection.indices.contains(destinationIndexPath.section){
                let idProjectDestination = self.idProjectInSection[destinationIndexPath.section]
                task!.idProject = Int32(idProjectDestination)
                Task.update(context: context, task: task!)
                self.loadTasks()
            }
        }
    }
}

extension AllTasksViewController: TaskTableViewProtocol {
    //Protocole lancement/arrêt d'une tache
    func onClickPausePlay(indexPath: IndexPath, cell: TaskTableViewCell) {
        let task = self.getTaskFromIndexPath(indexPath: indexPath)
        if task != nil{
            if !task!.quick{
                task!.pausePlay(context: context)
                Task.update(context: context, task: task!)
                self.loadTasks()
            }else{
                self.quickChoose(idTask: Int(task!.id))
            }
        }
    }
}

extension AllTasksViewController: UIActionSheetDelegate {
    //Action pour une quick task
    func actionSheet(_ actionSheet: UIActionSheet, clickedButtonAt buttonIndex: Int) {
        switch buttonIndex {
        case 1:
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let editTaskVC = storyboard.instantiateViewController(withIdentifier: "EditTaskViewController") as! EditTaskViewController
            editTaskVC.idTask = self.tempIdTask
            self.navigationController?.pushViewController(editTaskVC, animated: true)
        case 2:
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let chooseTaskVC = storyboard.instantiateViewController(withIdentifier: "ChooseTaskViewController") as! ChooseTaskViewController
            chooseTaskVC.idTask = self.tempIdTask
            self.navigationController?.pushViewController(chooseTaskVC, animated: true)
        case 3:
            Task.remove(context: context, idTask: self.tempIdTask)
            self.loadTasks()
        default:
            return
        }
    }
}
