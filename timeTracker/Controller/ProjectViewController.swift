//
//  ProjectViewController.swift
//  timeTracker
//
//  Created by tp on 19/03/2020.
//  Copyright © 2020 lanes. All rights reserved.
//

import Foundation
import UIKit
import CoreData

//Classe correspondante à l'écran d'affichage d'un projet
class ProjectViewController: UIViewController {
    
    var titleProject: String = ""
    var idProject: Int = 0
    var project: Project?
    var tempIdTask: Int = 0
    var tasks: [Task] = [];
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext

    
    @IBOutlet weak var projectNavigationBar: UINavigationItem!
    @IBOutlet weak var taskTableView: UITableView!
    @IBOutlet weak var editBarButton: UIBarButtonItem!
    //Action edit
    @IBAction func barButtonAction(_ sender: UIBarButtonItem) {
        if sender.tag == 1{
            if self.taskTableView.isEditing{
                editBarButton.title = "Edit"
                self.taskTableView.isEditing = false
            }else{
                editBarButton.title = "Done"
                self.taskTableView.isEditing = true
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.project = Project.getById(context: context, idProject: self.idProject)!
        self.projectNavigationBar.title = self.project!.title!
        self.taskTableView.allowsSelectionDuringEditing = true
        self.loadTasks()
    }
    
    //Segue pour aller au controller permettant d'ajouter une tache
    override func prepare (for segue: UIStoryboardSegue, sender: Any?){
        let createTaskVC = segue.destination as! CreateTaskViewController
        createTaskVC.projectId = idProject
    }
    
    //Recupère les tasks dans la BDD
    private func loadTasks(){
        self.tasks = Task.getByIdProject(context: context, idProject: idProject)
        taskTableView.reloadData()
    }
    
    //Choix action pour quick task
    private func quickChoose(idTask: Int){
        self.tempIdTask = idTask
        let actionSheet = UIActionSheet(title: " ", delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, otherButtonTitles: "Create new task", "Add to task", "Delete")
        actionSheet.show(in: self.view)
    }
}

extension ProjectViewController: UITableViewDataSource {
    //Retourne le nombre de section de la tableView
    func tableView(_ tableView: UITableView, numberOfSections section: Int) -> Int {
        return 1;
    }
    
    //return le nombre de cell par section
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tasks.count
    }
    
    //return le contenu d'une cell
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TaskCell", for: indexPath) as? TaskTableViewCell
        let task = self.tasks[indexPath.row]
        cell?.configure(context: context, delegate: self, index: indexPath, task: task)
        return cell!
    }
}

extension ProjectViewController: UITableViewDelegate {
    //Action lors de la sélection d'une ligne
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let task = tasks[indexPath.item]
        if(!task.quick){
            if(self.taskTableView.isEditing){
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let editTaskVC = storyboard.instantiateViewController(withIdentifier: "EditTaskViewController") as! EditTaskViewController
                taskTableView.deselectRow(at: indexPath, animated: true)
                editTaskVC.idTask = Int(task.id)
                self.navigationController?.pushViewController(editTaskVC, animated: true)
            }else{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let historyVC = storyboard.instantiateViewController(withIdentifier: "HistoryViewController") as! HistoryViewController
                taskTableView.deselectRow(at: indexPath, animated: true)
                historyVC.idTask = Int(task.id)
                self.navigationController?.pushViewController(historyVC, animated: true)
            }
        }else{
            self.quickChoose(idTask: Int(task.id))
        }
    }
    
    //Action lors d'une action de sélection
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        let task = tasks[indexPath.item]
        if editingStyle == .delete{
            Task.remove(context: context, idTask: Int(task.id))
            self.loadTasks()
        }
    }
}

extension ProjectViewController: TaskTableViewProtocol {
    //Protocole lancement/arrêt d'une tache
    func onClickPausePlay(indexPath: IndexPath, cell: TaskTableViewCell) {
        let task = tasks[indexPath.item]
        if !task.quick{
            task.pausePlay(context: context)
            Task.update(context: context, task: task)
            self.loadTasks()
        }else{
            self.quickChoose(idTask: Int(task.id))
        }
    }
}

extension ProjectViewController: UIActionSheetDelegate {
    //Action pour une quick task
    func actionSheet(_ actionSheet: UIActionSheet, clickedButtonAt buttonIndex: Int) {
        switch buttonIndex {
        case 1:
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let editTaskVC = storyboard.instantiateViewController(withIdentifier: "EditTaskViewController") as! EditTaskViewController
            editTaskVC.idTask = self.tempIdTask
            self.navigationController?.pushViewController(editTaskVC, animated: true)
        case 2:
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let chooseTaskVC = storyboard.instantiateViewController(withIdentifier: "ChooseTaskViewController") as! ChooseTaskViewController
            chooseTaskVC.idTask = self.tempIdTask
            self.navigationController?.pushViewController(chooseTaskVC, animated: true)
        case 3:
            Task.remove(context: context, idTask: self.tempIdTask)
            self.loadTasks()
        default:
            return
        }
    }
}
