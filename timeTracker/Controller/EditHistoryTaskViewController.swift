//
//  EditHistoryTaskViewController.swift
//  timeTracker
//
//  Created by tp on 15/04/2020.
//  Copyright © 2020 lanes. All rights reserved.
//

import Foundation
import UIKit
import CoreData

//Classe correspondante à l'écran pour éditer un historique de tache
class EditHistoryTaskViewController: UIViewController {

    var idHistoryTask: Int = 0
    var idTask = 0
    var historyTask: HistoryTask?
    var target = -1
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    @IBOutlet weak var editHistoryNavigationBar: UINavigationItem!
    //Action edit
    @IBAction func saveButtonAction(_ sender: UIBarButtonItem) {
        if self.historyTask!.id == -1{
            HistoryTask.add(context: context, idTask: Int(self.historyTask!.idTask), start: self.historyTask!.dateStart!, end: self.historyTask!.dateStop!)
            HistoryTask.remove(context: context, idHistoryTask: -1)
        }else{
            HistoryTask.update(context: context, historyTask: self.historyTask!)
        }
        navigationController?.popViewController(animated: true)
    }
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var itemHistoryTaskTableView: UITableView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if idHistoryTask == -1 {
            HistoryTask.remove(context: context, idHistoryTask: -1)
            self.historyTask = HistoryTask(context: context)
            self.historyTask!.id = -1
            self.historyTask!.idTask = Int32(self.idTask)
            self.historyTask!.dateStart = Date()
            self.historyTask!.dateStop = Date()
        }else{
            self.historyTask = HistoryTask.getById(context: context, id: idHistoryTask)
        }
        self.datePicker.addTarget(self, action: #selector(datePickerChanged(picker:)), for: .valueChanged)
        self.itemHistoryTaskTableView.reloadData()
    }
    
    //Action lorsque la date change
    @objc func datePickerChanged(picker: UIDatePicker){
        if self.target == 0 {
            historyTask?.dateStart! = self.datePicker.date
        }
        if self.target == 1 {
            historyTask?.dateStop! = self.datePicker.date
        }
        if self.target == 2 {
            let formatter = DateFormatter()
            formatter.dateFormat = "HH:mm:ss dd/MM/yyyy"
            let dateTemp = formatter.date(from: "00:00:00 01/01/2000")
            let intervalTemp = self.datePicker.date.timeIntervalSince(dateTemp!)
            historyTask?.dateStop! = (self.historyTask?.dateStart!.addingTimeInterval(intervalTemp))!
        }
        self.itemHistoryTaskTableView.reloadData()
    }
}

extension EditHistoryTaskViewController: UITableViewDataSource {
    //Retourne le nombre de section de la tableView
    func tableView(_ tableView: UITableView, numberOfSections section: Int) -> Int {
        return 1;
    }
    
    //return le nombre de cell par section
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3;
    }
    
    //return le contenu d'une cell
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EditHistoryTaskCell", for: indexPath)
        cell.backgroundColor = UIColor.white
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss dd/MM/yyyy"
        if indexPath.row == 0 {
            cell.textLabel?.text = "Start"
            cell.detailTextLabel!.text = formatter.string(from: historyTask!.dateStart!)
            if self.target == 0 {
                self.datePicker.datePickerMode = .dateAndTime
                self.datePicker.setDate(historyTask!.dateStart!, animated: true)
                cell.backgroundColor = UIColor.cyan
            }
        }
        if indexPath.row == 1 {
            cell.textLabel?.text = "Stop"
            cell.detailTextLabel!.text = formatter.string(from: historyTask!.dateStop!)
            if self.target == 1 {
                self.datePicker.datePickerMode = .dateAndTime
                self.datePicker.setDate(historyTask!.dateStop!, animated: true)
                cell.backgroundColor = UIColor.cyan
            }
        }
        if indexPath.row == 2 {
            let timer = -Int(historyTask!.dateStart!.timeIntervalSince(historyTask!.dateStop!))
            var s = String(timer-(60*(timer/60)))
            var m = String((timer-(3600*(timer/3600)))/60)
            var h = String(timer/3600)
            if s.count == 1 {
                s = "0\(s)"
            }
            if m.count == 1 {
                m = "0\(m)"
            }
            if h.count == 1 {
                h = "0\(h)"
            }
            cell.textLabel?.text = "Duration"
            cell.detailTextLabel!.text = "\(h):\(m):\(s)"
            if self.target == 2 {
                cell.backgroundColor = UIColor.cyan
                self.datePicker.datePickerMode = .countDownTimer
                let dateTemp = formatter.date(from: "\(h):\(m):\(s) 01/01/2000")
                self.datePicker.setDate(dateTemp!, animated: true)
            }
        }
        return cell
    }
}

extension EditHistoryTaskViewController: UITableViewDelegate {
    //Action lors de la sélection d'une ligne
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.itemHistoryTaskTableView.deselectRow(at: indexPath, animated: true)
        self.target = indexPath.row
        self.itemHistoryTaskTableView.reloadData()
    }
    
}
