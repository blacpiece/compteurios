//
//  HistoryViewController.swift
//  timeTracker
//
//  Created by tp on 15/04/2020.
//  Copyright © 2020 lanes. All rights reserved.
//

import UIKit
import CoreData

//Classe correspondante à l'écran pour afficher l'historique d'une tache
class HistoryViewController: UIViewController {

    var idTask: Int = 0
    var task: Task?
    var historyTasks: [[HistoryTask]] = []
    var headSection: [String] = []
    var nbInSection: [Int] = []
    var count = 0
    var timer: Timer!
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    @IBOutlet weak var historyTaskTableView: UITableView!
    @IBOutlet weak var historyNavigationBar: UINavigationItem!
    @IBOutlet weak var totalLabel: UILabel!
    //Action Edit
    @IBAction func barButtonAction(_ sender: UIBarButtonItem) {
        if sender.tag == 1 {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let editHistoryTaskVC = storyboard.instantiateViewController(withIdentifier: "EditHistoryTaskViewController") as! EditHistoryTaskViewController
            editHistoryTaskVC.idHistoryTask = -1
            editHistoryTaskVC.idTask = self.idTask
            self.navigationController?.pushViewController(editHistoryTaskVC, animated: true)
        }
        if sender.tag == 2 {
            if (self.task?.play)!{
                self.task?.pausePlay(context: context)
            }
            HistoryTask.removeByIdTask(context: context, idTask: idTask)
            self.loadHistoryTasks()
            historyTaskTableView.reloadData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.task = Task.getById(context: context, idTask: self.idTask)!
        self.historyNavigationBar.title = self.task!.title!
        self.loadHistoryTasks()
    }
    
    //Recupère les historyTasks dans la BDD
    private func loadHistoryTasks(){
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        var dateSection: String = "-1"
        var dateHistoryTask: String = "-1"
        var indexSection = -1;
        HistoryTask.remove(context: context, idHistoryTask: -1)
        var tempHistoryTasks = HistoryTask.getByIdTask(context: context, idTask: idTask)
        if self.task!.play{
            let newHistoryTask = HistoryTask(context: context)
            newHistoryTask.id = -1
            newHistoryTask.idTask = task!.id
            newHistoryTask.dateStart = task!.lastStart
            newHistoryTask.dateStop = nil
            tempHistoryTasks.append(newHistoryTask)
        }
        tempHistoryTasks.sort {
            if $0.dateStart?.compare($1.dateStart!) != .orderedAscending{
                return true
            }else{
                return false
            }
        }
        
        self.headSection = []
        self.nbInSection = []
        self.historyTasks = []

        for historyTask in tempHistoryTasks{
            dateHistoryTask = formatter.string(from: historyTask.dateStart!)
            if dateHistoryTask != dateSection {
                indexSection += 1
                dateSection = dateHistoryTask
                self.headSection.append(dateSection)
                self.nbInSection.append(1)
                self.historyTasks.append([historyTask])
            }else{
                self.nbInSection[indexSection] += 1
                self.historyTasks[indexSection].append(historyTask)
            }
        }
        self.setupTimer()
        historyTaskTableView.reloadData()
    }
    
    //Récupère l historyTask correspondante à un indexPath
    private func getHistoryTaskFromIndexPath(indexPath: IndexPath)-> HistoryTask?{
        if self.historyTasks.indices.contains(indexPath.section){
            let tempHistoryTasks = self.historyTasks[indexPath.section]
            if tempHistoryTasks.indices.contains(indexPath.row){
                let historyTask = tempHistoryTasks[indexPath.row]
                return historyTask
            }
        }
        return nil
    }
    
    //Paramétrage du timer de la tache
    private func setupTimer(){
        self.count = self.task!.getCount(context: context)
        if self.task!.play {
            self.stop()
            self.start()
            self.totalLabel.textColor = UIColor.red
        }else{
            self.stop()
            self.totalLabel.textColor = UIColor.black
        }
        self.timerToString()
    }
    
    //Timer to string
    private func timerToString(){
        var s = String(self.count-(60*(self.count/60)))
        var m = String((self.count-(3600*(self.count/3600)))/60)
        var h = String(self.count/3600)
        if s.count == 1 {
            s = "0\(s)"
        }
        if m.count == 1 {
            m = "0\(m)"
        }
        if h.count == 1 {
            h = "0\(h)"
        }
        self.totalLabel.text = "Total: \(h):\(m):\(s)"
    }
    
    private func start(){
        self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(increment), userInfo: nil, repeats: true)
    }
    
    private func stop(){
        if self.timer != nil{
            self.timer.invalidate()
        }
        self.timerToString()
    }
    
    @objc func increment(){
        self.count+=1
        self.timerToString()
    }

}

extension HistoryViewController: UITableViewDataSource {
    //Retourne le nombre de section de la tableView
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.headSection.count;
    }
    
    //Retourne le nom de la section
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if self.headSection.indices.contains(section){
            return self.headSection[section]
        }else{
            return "HistoryTask"
        }
    }
    
    //return le nombre de cell par section
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.nbInSection.indices.contains(section){
            return self.nbInSection[section]
        }else{
            return 0
        }
    }
    
    //return le contenu d'une cell
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryTaskCell", for: indexPath) as? HistoryTaskTableViewCell
        let historyTask = self.getHistoryTaskFromIndexPath(indexPath: indexPath)
        if historyTask != nil{
            cell?.configure(historyTask: historyTask!)
        }
        return cell!
    }
}
    
extension HistoryViewController: UITableViewDelegate {
    //Action lors de la sélection d'une ligne
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let historyTask = self.getHistoryTaskFromIndexPath(indexPath: indexPath)
        if historyTask != nil && historyTask?.dateStop != nil{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let editHistoryTaskVC = storyboard.instantiateViewController(withIdentifier: "EditHistoryTaskViewController") as! EditHistoryTaskViewController
            historyTaskTableView.deselectRow(at: indexPath, animated: true)
            editHistoryTaskVC.idHistoryTask = Int(historyTask!.id)
            editHistoryTaskVC.idTask = self.idTask
            self.navigationController?.pushViewController(editHistoryTaskVC, animated: true)
        }
    }
    
    //Action lors d'une action d'édition
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        let historyTask = self.getHistoryTaskFromIndexPath(indexPath: indexPath)
        if editingStyle == .delete{
            HistoryTask.remove(context: context, idHistoryTask: Int(historyTask!.id))
            self.loadHistoryTasks()
        }
    }
}
